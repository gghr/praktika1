import React, { Component } from 'react'
import Carousel from 'react-bootstrap/Carousel'
import forestImg from '../assets/1.jpg';
import dforestImg from '../assets/2.jpg';
import fforestImg from '../assets/3.jpeg';
export default class CarouselBox extends Component {
  render() {
    return (
        <Carousel>
            <Carousel.Item style={{'height': '600px'}}>
                <img
                    className='d-block w-100'
                    src={forestImg}
                    alt='Forest'
                />
                <Carousel.Caption>
                    <h3>Горячие туры</h3>
                    <p></p>

                 </Carousel.Caption>
            </Carousel.Item>


            <Carousel.Item style={{'height': '600px'}}>
                <img
                    className='d-block w-100'
                    src={dforestImg}
                    alt='dForest'
                />
                <Carousel.Caption>
                    <h3>Горячие туры</h3>
                    <p></p>

                 </Carousel.Caption>
             </Carousel.Item>


             <Carousel.Item style={{'height': '600px'}}>
                <img
                    className='d-block w-100'
                    src={fforestImg}
                    alt='fForest'
                />
                <Carousel.Caption>
                    <h3>Горячие туры</h3>
                    <p></p>

                </Carousel.Caption>
            </Carousel.Item>

        </Carousel>
    )
  }
}
