import React, { Component } from 'react';
import { Navbar, Container, Nav, NavLink } from 'react-bootstrap'
import logo from './logo192.png'
import styled from 'styled-components';



import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link
  } from "react-router-dom";

import Home from '../pages/Home.js'
import About from '../pages/About.js'
import Contact from '../pages/Contact.js'

const Styles = styled.div `
    a, .navbar-brand, .navbar-nav .nav-link {
        color:#adb1b8;
        &:hover {
            color: black
        }
    }
    `

export default class Header extends Component {

    render() {
        return (
            <>
            <Styles>
                <Router>            
                <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
                    <Container>
                      
                          
                        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                        <Navbar.Collapse id="responsive-navbar-nav" >
                            <Nav className="me-auto">
                                <NavLink as={Link} to ="/">
                                <Navbar.Brand><img
                                src={logo}
                                height="30"
                                width="30"
                                className="d-inline-block align-top"
                                alt="logo"
                            />Go</Navbar.Brand>
                            </NavLink>
                                <NavLink as={Link} to="/about">Туры</NavLink>
                                <NavLink as={Link} to="/contact">Контакты</NavLink>
                                
                                
                            </Nav>
                            
                            
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
                

                    <Routes>
                        <Route path='/' element={<Home/>}/>
                        <Route path='/about' element={<About/>}/>
                        <Route path='/contact' element={<Contact/>}/>
                    
                    </Routes>
                </Router>
                </Styles>


            </>
        )
    }

}