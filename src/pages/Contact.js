import React, { Component } from 'react'
import { Form, Button, Container } from 'react-bootstrap'

export default class Contact extends Component {
  render() {
    return (
      <Container style={{width: '500px'}}>
        <h1 className='text-center'> Свяжитесь с нами </h1>
        <Form>
          <Form.Group controlId='formBaicEmail'>
            <Form.Label>Email адрес</Form.Label>
            <Form.Control type="email" placeholder='Введите email'/>
            
          </Form.Group>

          <Form.Group controlId='formBaicPassword'>
            <Form.Label>Введите сообщение</Form.Label>
            <Form.Control as="textarea" rows="3" />
          </Form.Group>

        
          <Button variant='primary' type='submit' >Отправить</Button>

        </Form>
      </Container>
    )
  }
}
