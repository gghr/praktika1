import React, { Component } from 'react'
import CarouselBox from '../components/CarouselBox'
import { Container, Card, Row, Col } from 'react-bootstrap'
import Footer from '../components/Footer';

export default class home extends Component {
  render() {
    return (
    
      
      <> 
        <CarouselBox />

        

        <Container style={{paddingTop: '2rem', paddingBottom: '2rem'}}>
          <Row>
            <Col>
          
            <Card style={{width:'18rem'}} bg='white' border='dark'>
              <Card.Img
                variant="top"
                width={50}
                
                src={'https://www.clipartmax.com/png/full/278-2784971_tag-computer-icons-clip-art-price-clipart-black-and-white.png'}
              />
              <Card.Body>
                <Card.Title>Выгодно</Card.Title>
                <Card.Text>
                  Туры по низким ценам, без комиссий и скрытых услуг
                </Card.Text>
             
              </Card.Body>
            </Card>
            </Col>

            <Col>
          
            <Card style={{width:'18rem'}} bg='white' border='dark'>
              <Card.Img
                variant="top"
                src='https://sun6-21.userapi.com/s/v1/ig2/tsqvuUa1J3S47CzEovlAs9OWh5YGX2a83yJyZAol_tvqo5uGMJ5MkW0bga5xOvF9YIqwTfBYzC3Iu1426wDGmCv3.jpg?size=1920x1920&quality=95&crop=124,0,1920,1920&ava=1'
              />
              <Card.Body>
                <Card.Title>Познавательно</Card.Title>
                <Card.Text>
                  Наши лучшие гиды расскажут вам о всех достопримечательностях
                </Card.Text>
             
              </Card.Body>
            </Card>
            </Col>

            <Col>
          
            <Card style={{width:'18rem'}} bg='white' border='dark'>
              <Card.Img
          
                variant="top"
                src='https://static.tildacdn.com/tild6539-6265-4135-b561-306339353436/download-png-1024px-.png'
              />
              <Card.Body>
                <Card.Title>Безопасно</Card.Title>
                <Card.Text>
                  Наша компания гарантирует, что ваша жизнь будет под защитой
                </Card.Text>
             
              </Card.Body>
              </Card>
            </Col>
            </Row>




          





        </Container>
    
      
        <Footer/>
    </>)
  }
}
